import java.util.ArrayList;

public class Bank implements BankOperation {

    ArrayList<Account> accountList = new ArrayList<Account>();
    Integer accountIndex = 0;

    @Override
    public int createAccount() {
        Account acc = new Account(accountIndex+1, 0);
        accountList.add(accountIndex, acc);
        if(this.accountList.isEmpty()) {
            accountIndex = 1;
            return 1;
        } else {
            //return accountList.size();
            accountIndex += 1;
            return accountIndex;
        }

    }
    @Override
    public int deleteAccount(int accountNumber) {
        try {
            Account acc = accountList.get(accountNumber);
            if(acc == null) {
                return ACCOUNT_NOT_EXISTS;
            } else {
                Integer balance = acc.getBalance();
                accountList.set(accountNumber-1, null);
                return balance;
            }
        } catch (IndexOutOfBoundsException e) {
            return ACCOUNT_NOT_EXISTS;
        }
    }

    @Override
    public boolean deposit(int accountNumber, int amount) {
        if(amount < 0) {
            return false;
        }
        try {
            Account acc = accountList.get(accountNumber-1);
            if(acc == null || acc.getBalance() == -1 || acc.getAccountNumber() == -1) {
                return false;
            } else {
                acc.setBalance(acc.getBalance() + amount);
                return true;
            }
        } catch (IndexOutOfBoundsException e) {
            return false;
        }

    }
    @Override
    public boolean withdraw(int accountNumber, int amount) {

        if(amount < 0) {
            return false;
        }
        try {
            Account acc = accountList.get(accountNumber-1);
            if(acc == null) {
                return false;
            } else if(acc.getBalance() > amount) {
                acc.setBalance(acc.getBalance() - amount);
                return true;
            } else {
                return false;
            }
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
    }

    @Override
    public boolean transfer(int fromAccount, int toAccount, int amount) {

        try {


            if (accountList.get(fromAccount - 1) == null || accountList.get(toAccount - 1) == null) {
                return false;
            } else {

            }
            if (amount < 0) {
                return false;
            }

                Account accFrom = accountList.get(fromAccount - 1);
                Account accTo = accountList.get(toAccount - 1);

                if (amount < 0) {
                    return false;
                }
                try {
                    if (accFrom == null || accTo == null) {
                        return false;
                    } else if (accFrom.getBalance() > amount) {
                        accFrom.setBalance(accFrom.getBalance() - amount);
                        if (accFrom == null || accTo == null) {
                            return false;
                        } else {
                            accTo.setBalance(accTo.getBalance() + amount);
                            return true;
                        }
                    } else {
                        return false;
                    }
                } catch (IndexOutOfBoundsException e) {
                    return false;
                }
        } catch (Exception e) {
            return false;
        }
    }
    @Override
    public int accountBalance(int accountNumber) {
        try {
            Account acc = accountList.get(accountNumber-1);
            if(acc == null) {
                return ACCOUNT_NOT_EXISTS;
            } else {
                return acc.getBalance();
            }
        } catch (IndexOutOfBoundsException e) {
            return ACCOUNT_NOT_EXISTS;
        }


    }
    @Override
    public int sumAccountsBalance() {
        Integer sum = 0;
        for(Account a : accountList) {
            if(a != null) {
                sum += a.getBalance();
            }
        }
        return sum;
    }
}